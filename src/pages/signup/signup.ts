import { Component } from '@angular/core';
import {  NavController} from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { LoginPage } from '../login/login';
import { AuthService } from "../../providers/auth-service/auth-service";

@Component({
    selector: 'page-signup',
    templateUrl: 'signup.html',
})
export class SignupPage {
    responseData : any;
    userData = {"name": "", "email": "","password": ""};

    constructor(public navCtrl: NavController, public authService:AuthService ) {
    }

    signup(){
        // this.authService.postData(this.userData,'signup').then((result) => {
        //     this.responseData = result;
        //     if(this.responseData.userData){
        //         console.log(this.responseData);
        //         localStorage.setItem('userData', JSON.stringify(this.responseData));
        //         this.navCtrl.push(TabsPage);
        //     }
        //     else{ console.log("User already exists"); }
        // }, (err) => {
        //     // Error log
        // });


        this.authService.postData(this.userData,"register").then((result) =>{
            this.responseData = result;
            console.log(this.responseData);
            localStorage.setItem('userData',JSON.stringify(this.responseData) )
            this.navCtrl.push(TabsPage);
        },(err)=>{

            }
        );

        // this.navCtrl.push(TabsPage);

    }

    login(){
        //Login page link
        this.navCtrl.push(LoginPage);
    }
}